package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String INSERT_NEW_USER = "INSERT INTO users (login) VALUES (?)";
    private static final String INSERT_NEW_USER_TEAMS = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
    private static final String INSERT_NEW_TEAM = "INSERT INTO teams (name) VALUES (?)";
    private static final String SELECT_USER = "SELECT * FROM users WHERE login = ?";
    private static final String SELECT_TEAM = "SELECT * FROM teams WHERE name = ?";
    private static final String UPDATE_TEAM_ID = "UPDATE teams SET name = ? WHERE id = ?";
    private static final String SELECT_USER_TEAMS = "SELECT t.* FROM teams AS t " +
            "JOIN users_teams AS ut ON t.id = ut.team_id WHERE ut.user_id = ?";
    private static final String GET_TEAMS = "SELECT * FROM teams";
    private static final String GET_USERS = "SELECT * FROM users";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    private static final String DELETE_USER = "DELETE FROM users WHERE id = ?";


    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        return new DBManager();
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             Statement preparedStatement = connection.createStatement();
             ResultSet rs = preparedStatement.executeQuery(GET_USERS);
        ) {
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(" ", e);
        }
        return users;
    }


    public static String get_URL() {
        Properties properties = new Properties();
        String url = "";
        try (FileInputStream fileInputStream = new FileInputStream("app.properties")) {
            properties.load(fileInputStream);
            url = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_USER, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             PreparedStatement pr = connection.prepareStatement(DELETE_USER);) {
            for (int i = 0; i < users.length; i++) {
                pr.setInt(1, users[i].getId());
                pr.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER);
        ) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = User.createUser(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        ResultSet resultSet = null;
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TEAM)
        ) {
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            Team team;
            while (resultSet.next()) {
                team = Team.createTeam(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                return team;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                close(resultSet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             Statement preparedStatement = connection.createStatement();
             ResultSet rs = preparedStatement.executeQuery(GET_TEAMS);
        ) {
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(" ", e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEW_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        PreparedStatement pr = null;
        try {
            connection = DriverManager.getConnection(DBManager.get_URL());
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            pr = connection.prepareStatement(INSERT_NEW_USER_TEAMS);
            for (Team team : teams) {
                pr.setInt(1, user.getId());
                pr.setInt(2, team.getId());
                pr.execute();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                rollBack(connection);
                throw new DBException("ex", e);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                close(pr);
                close(connection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void close(AutoCloseable connection) throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

    private void rollBack(Connection connection) throws SQLException {
        if (connection != null) {
            connection.rollback();
        }
    }


    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection connection = null;
        PreparedStatement pr = null;
        ResultSet rs = null;
        try {
            connection = DriverManager.getConnection(DBManager.get_URL());
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            pr = connection.prepareStatement(SELECT_USER_TEAMS);
            pr.setInt(1, user.getId());
            rs = pr.executeQuery();
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                rollBack(connection);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                close(rs);
                close(pr);
                close(connection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return teams;
    }


    public boolean deleteTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             PreparedStatement pr = connection.prepareStatement(DELETE_TEAM);) {
            pr.setInt(1, team.getId());
            pr.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(DBManager.get_URL());
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TEAM_ID)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(" ", e);
        }
        return true;
    }
}
